/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.tictactoe;

import java.util.Scanner;

/**
 *
 * @author Thanya
 */
public class TicTacToe {
    static char table[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer = 'O';
    static int row,col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;
    
    public static void main(String[] args) {
        
        showWelcome();
        while(true) {
        showTable();
        showTurn();
        inputRowCol();
        process();
        if(finish) {
            break;
        }
      }
        
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
       for(int r = 0;r < table.length;r++){
           for(int c = 0;c < table.length;c++){
               System.out.print(table[r][c]);
           }
           System.out.println("");
       }
    }

    public static void showTurn() {
        System.out.println("Turn "+currentPlayer);
    }

    private static void inputRowCol() {
        System.out.println("Please input row, col:");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    private static void process() {
        if(setTable()){
            if(checkWin()){
                finish = true;
                showTable();
                System.out.println(">>>"+currentPlayer+" Win<<<");
                return;
            }
            if(count == 8) {
                finish = true;
                showTable();
                System.out.println(">>> Draw <<<");
                return;
            }
            count++;
            switchPlayer();
        }
        
    }
    
    public static void switchPlayer(){
        if(currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }
    
    public static boolean setTable(){
        table[row-1][col-1] = currentPlayer;
        return true;
    }

    private static boolean checkWin() {
        if(checkVertical()) {
            return true;
        } else if(checkHorizontal()) {
            return true;
        } else if(checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkVertical() {
        for(int r=0;r<table.length;r++) {
            if(table[r][col-1] != currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkHorizontal() {
        for(int c=0;c<table.length;c++) {
            if(table[row-1][c] != currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkX() {
        if(checkX1()) {
            return true;
        } else if(checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for(int i=0;i<table.length;i++) {
            if(table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for(int i=0;i<table.length;i++) {
            if(table[i][2-i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

}
